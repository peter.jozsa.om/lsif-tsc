FROM node:14-alpine

RUN apk add --no-cache bash git && \
    npm i -g @sourcegraph/lsif-tsc

ENTRYPOINT [ "/bin/bash", "-O", "globstar" ]