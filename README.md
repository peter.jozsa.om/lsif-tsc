# TypeScript LSIF indexer

TypeScript LSIF indexer [@sourcegraph/lsif-node](https://github.com/sourcegraph/lsif-node) NPM package packed into an Alpine based Docker image for LSIF indexing your Node.js/TypeScript source code.